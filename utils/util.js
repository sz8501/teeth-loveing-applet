const formatTime = date => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()

    return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
    n = n.toString()
    return n[1] ? n : `0${n}`
}

const getStorageSync = key => {
    let o = wx.getStorageSync(key);
    if (o) {
        return JSON.parse(o)
    } else {
        return {}
    }
}

// 校验手机号
const checkPhone = phone => {
    let reg = /^(13[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9]|14[0-9])[0-9]{8}$/
    let pass = !reg.test(phone)
    return pass
}

// 富文本给image设置class，防止图片过大溢出
var formatRichText = function (str) {
    if (!str) {
        return ''
    }
    console.log(typeof str);
    let m = str.replace(/<img/g, '<img class="richImg"')
    return m;
}

// 计算经纬度距离  lng、lat：用户经纬度   tlng、tlat：医院目标经纬度
var getDistance = function (lng, lat, tlng, tlat) {
    if (!lng || !lat || !tlng || !tlat) {
        return ''
    }
    var radLat1 = lat * Math.PI / 180.0;
    var radLat2 = tlat * Math.PI / 180.0;
    var a = radLat1 - radLat2;
    var b = lng * Math.PI / 180.0 - tlng * Math.PI / 180.0;
    var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
        Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
    s = s * 6378.137; // EARTH_RADIUS;
    s = Math.round(s * 10000) / 10000;
    s = s.toFixed(2)
    return s;
}


// 提示语
const showToast = options => {
    let toast = wx.showToast({
        title: options.message || options.title,
        icon: options.icon || 'none',
        success: function () {
            if (options.success) {
                (options.success)()
            }
        }
    })
    return toast
}

// 弹框
const showModal = options => {
    let showCancel = true
    if (options.showCancel === false) {
        showCancel = options.showCancel
    }
    let _modal = wx.showModal({
        title: options.title || '',
        content: options.content || options.message || '',
        showCancel: showCancel,
        cancelText: options.cancelText || '取消',
        confirmText: options.confirmText || '确定',
        success: function (res) {
            if (res.confirm) {
                if (options.success) {
                    (options.success)()
                }
            } else {
                if (options.cancel) {
                    (options.cancel)()
                }
            }
        },
        fail: function () {
            if (options.fail) {
                (options.fail)()
            }
        }

    })
    return _modal
}

// 判断是否登录
const judageLogin = () => {
    let mobile = wx.getStorageSync('mobile');
    if (mobile) {
        return true;
    } else {
        return false;
    }
}

module.exports = {
    formatTime,
    getStorageSync,
    checkPhone,
    formatRichText,
    getDistance,
    showModal,
    showToast,
    judageLogin
}