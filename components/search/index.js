// components/search/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        placeholder: {
            type: String,
            value: '医生姓名/症状/自锁/隐适美'
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        term: ''
    },

    /**
     * 组件的方法列表
     */
    methods: {
        // 监听输入方法
        onInput(e) {
            //this.triggerEvent('input', e.detail.value)
            console.log(e);
            this.setData({
                term: e.detail.value
            })
            
        },

        //监听输入完点击完成
        onComfirm(e) {
            let txt = e.detail.value
            if (!txt) {
                return false;
            }
            this.triggerEvent('update', txt);
            this.triggerEvent('confirm', txt)
        },

        // 监听输入框失去焦点
        onBlur(e) {
            this.triggerEvent('blur', e.detail.value)
        },

        // 清除搜索输入
        clearSearch() {
            console.log(this.data.term);
            if (this.data.term) {
                this.setData({
                    term: ''
                })
                this.triggerEvent('cancel', '')
                //this.triggerEvent('update', this.data.term);
                return false;
            }
            wx.navigateBack();

        },
    }
})