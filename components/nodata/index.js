// components/nodata/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        tips: '暂无数据'
    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})