// components/fixedButton/index.js
var app = getApp()
Component({

    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        safeHeight:app.globalData.safeHeight,
    },

    /**
     * 组件的方法列表
     */
    methods: {
        // 立即预约点击事件
        clickEvent(){
            this.triggerEvent('click')
        }
    }
})