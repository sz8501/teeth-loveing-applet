// components/case/index.js
import {
    judageLogin
} from '@/utils/util';
Component({
    options: {
        styleIsolation: 'apply-shared'
    },

    /**
     * 组件的属性列表
     */
    properties: {
        info: {
            type: Object,
            value: {},
            observer: function (n) {
                let arr = [];
                if (n.caseImageUrl) {
                    arr = n.caseImageUrl.split(',')
                }
                console.log(arr);
                this.setData({
                    oInfo: n,
                    coverImage: arr[0] || ''
                })
            }
        },
        doctorId: {
            type: String,
            value: ''
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        oInfo: {},
        coverImage: ''
    },

    /**
     * 组件的方法列表
     */
    methods: {

        // 查看详情
        lookDetail() {
            let row = this.data.oInfo;
            let isLogin = judageLogin();
            let url = encodeURIComponent(`/pages/case/detail?id=${row.id}&doctorId=${row.doctorId}`);

            wx.navigateTo({
              url: `/pages/case/detail?id=${row.id}&doctorId=${row.doctorId}`,
          })
          return false;
          // 2024-03-04 下面临时注释

            if (!isLogin) {
                wx.navigateTo({
                    url: `/pages/login/index?url=${url}`,
                })
                return false;
            }
            wx.navigateTo({
                url: `/pages/case/detail?id=${row.id}&doctorId=${row.doctorId}`,
            })
        }

    }
})