// components/doctor/index.js
import {
    getDistance,
    judageLogin
} from '@/utils/util';
import moment from 'moment';
Component({

    /**
     * 组件的属性列表
     */
    properties: {
        info: {
            type: Object,
            value: {},
            observer: function (n) {
                if (n) {
                    let _hlist = n.hospitalList || [];
                    let _first = _hlist[0] || {}
                    if (_first.longitude) {
                        n.meter = this.getMeter(_first.longitude, _first.latitude);
                    }
                }
                this.setData({
                    oInfo: n || {}
                })
            }
        },
        keyword: {
            type: [String, Number],
            value: '',
            observer: function (n) {
                this.setData({
                    searchText: n || ''
                })
            }
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        oInfo: {},
        searchText: ''
    },

    /**
     * 组件的方法列表
     */
    methods: {
        // 计算距离
        getMeter(lng, lat) {
            let ulng = wx.getStorageSync('lng');
            let ulat = wx.getStorageSync('lat');
            if (!lng || !lat) {
                return ''
            }

            let m = getDistance(ulng, ulat, lng, lat);
            return m;

        },

        // 查看详情
        lookDetail() {
            let isLogin = judageLogin();
            let row = this.data.oInfo;
            let url = encodeURIComponent(`/pages/doctor/detail?id=${row.id}`);
            let time = new Date().getTime(); // 获取当前时间的毫秒数
            let expiredTime = wx.getStorageSync('expiredTime');
            //let pageUrl = `/pages/doctor/detail?id=${row.id}`;
            let pageUrl = `/pages/login/index?url=${url}`;
            console.log(time > expiredTime);
            wx.navigateTo({
              url: `/pages/doctor/detail?id=${row.id}`,
          })
          return false;
          // 2024-03-04 下面临时注释
            // 1、未登录  expiredTime是否存在，如果未登录+ 当前时间小于失效时间，则跳转到详情页面， 未登录+当前时间大于失效时间  则跳转登录页面
            if (!isLogin) {
                // 未登录 + 当前时间大于失效时间
                if (expiredTime && time < expiredTime) {
                    pageUrl = `/pages/doctor/detail?id=${row.id}`;
                }
            }else{
                pageUrl = `/pages/doctor/detail?id=${row.id}`;
            }
            wx.navigateTo({
                url: pageUrl,
            })
        },
    }
})