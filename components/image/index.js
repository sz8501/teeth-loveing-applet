// components/image/index.js
Component({
    externalClasses: ['custom-class', 'class'],
    /**
     * 组件的属性列表
     */
    properties: {
        src: {
            type: String,
            value: '',
            observer: function (n) {
                if (n) {
                    let src = n
                    let prefix = 'https://llkj-initialize.oss-cn-shenzhen.aliyuncs.com/teeth/'
                    this.setData({
                        imagePath: `${prefix}${src}`
                    })
                }
            }
        },
        mode: {
            type: String,
            value: 'scaleToFill'
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        imagePath: ''
    },

    // 组件生命周期
    lifetimes: {
        // 在组件实例进入页面节点树时执行
        attached: function () {
            // let src = this.data.src
            // let prefix = 'https://llkj-initialize.oss-cn-shenzhen.aliyuncs.com/hospital/'
            // this.setData({
            //     imagePath: `${prefix}${src}`
            // })
        },
        // 在组件实例被从页面节点树移除时执行
        detached: function () {

        }
    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})