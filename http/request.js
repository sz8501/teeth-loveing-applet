import {
    ERRTIPMSG,
    BASE_URL
} from './config'
console.log(ERRTIPMSG)
let hasModal = null
const setHeader = header => {
    // 本地获取存储的sessionId
    var sessionId = wx.getStorageSync('JSESSIONID');
    var cookies = header['Set-Cookie'];
    if (cookies) {
        var n = cookies ? cookies.split(';') : [];
        sessionId = n[0];
        if (sessionId) {
            // 从cookie中获取sessionId存储到本地
            wx.setStorageSync('JSESSIONID', sessionId);
        }
    }
    if (sessionId) {
        header['Cookie'] = sessionId;
    }
    return header;
}

const request = options => {

    let code = 200
    let header = {};
    let token = wx.getStorageSync('token');
    if (options.code) {
        code = options.code
    }
    if (token) {
        header['Authorization'] = token;
    };
    if (options.loading !== false) {
        wx.showLoading({
            title: '加载中...',
            mask: true
        });
    }

    return new Promise((resolve, reject) => {
        wx.request({
            url: BASE_URL + options.url,
            data: options.data,
            header: header,
            method: options.method || 'GET',
            success: function (res) {
                //收到开发者服务成功返回的回调函数
                var obj = res.data;
                setHeader(res.header);
                setTimeout(function () {
                    //增加setTimeout,防止wx.hideLoading 关闭掉wx.showToast
                    if (obj.code != code) {
                        resolve(res.data)
                        if (obj.code == "603" || obj.code == "604") {
                            if (hasModal) {
                                return false;
                            }
                            console.log(hasModal)
                            hasModal = wx.showModal({
                                title: '提示',
                                content: obj.message || ERRTIPMSG,
                                showCancel: false,
                                success() {
                                    hasModal = null;
                                    wx.reLaunch({
                                        url: '/pages/login/index',
                                    });
                                    wx.clearStorageSync();
                                }
                            })
                        } else {
                            // wx.showToast({
                            //     title: obj.message || ERRTIPMSG,
                            //     icon: 'none'
                            // })
                            console.log(obj.message)
                            hasModal = wx.showModal({
                                title: '提示',
                                content: obj.message || ERRTIPMSG,
                                showCancel: false
                            })
                        }
                    } else {
                        resolve(res.data)
                    }
                }, 2);
            },
            fail: function (res) {
                console.log('55555')
                //接口调用失败的回调函数
                setTimeout(function () {
                    //增加setTimeout,防止wx.hideLoading 关闭掉wx.showToast
                    reject(res.data)
                }, 2)
            },
            complete: function (res) {
                //接口调用结束的回调函数（调用成功、失败都会执行）
                if (options.loading !== false) {
                    wx.hideLoading();
                }
            }
        });
    })
}

module.exports = request