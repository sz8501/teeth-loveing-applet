/**
 * 接口环境
 * develop:开发环境
 * trial：测试环境(体验版)
 * release：生产环境
 */
const accountInfo = wx.getAccountInfoSync();
const environment = accountInfo.miniProgram.envVersion
console.log(environment)

// 请求接口地址列表
const POST_URL_LIST = {
    'develop': 'https://data.lilikq.com/ayc/',
    'trial': 'https://data.lilikq.com/ayc/',
    'release': 'https://data.lilikq.com/ayc/'
}

console.log(POST_URL_LIST[environment])
// 请求接口地址
export const BASE_URL = POST_URL_LIST[environment]

// 图片路径
export const IMAGE_PATH = BASE_URL

// 错误提示语
export const ERRTIPMSG = '出错了，请联系管理员'