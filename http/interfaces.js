import {
    BASE_URL
} from './config'
import request from './request'
const prefix = ''

// 获取城市列表
export const GET_CITY_LIST = params => {
    return request({
        url: `${prefix}city/pageQuery`,
        data: params,
        method: 'POST'
    })
}

// 获取资讯
export const GET_ARTICLE_LIST = params => {
    return request({
        url: `${prefix}article/pageQuery`,
        data: params,
        method: 'POST'
    })
}

// 获取资讯详情
export const GET_ARTICLE_INFO = params => {
    return request({
        url: `${prefix}article/${params.id}`,
        data: {},
        method: 'GET'
    })
}

// 查询报价
export const GET_INQUIRE_PRICE = params => {
    return request({
        url: `${prefix}query-price/add`,
        data: params,
        method: 'POST'
    })
}

// 查询医生列表
export const GET_DOCTOR_LIST = params => {
    return request({
        url: `${prefix}doctor/pageQuery`,
        data: params,
        method: 'POST'
    })
}

// 查询医生详情
export const GET_DOCTOR_INFO = params => {
    return request({
        url: `${prefix}doctor/${params.id}`,
        data: {},
        method: 'GET'
    })
}

// 查询案例列表
export const GET_CASE_LIST = params => {
    return request({
        url: `${prefix}case/pageQuery`,
        data: params,
        method: 'POST'
    })
}

//查询案例详情
export const GET_CASE_INFO = params => {
    return request({
        url: `${prefix}case/${params.id}`,
        data: {},
        method: 'GET'
    })
}

// 微信登录接口
export const LOGIN_BY_WEIXIN = params => {
    let options = {
        ...params
    };
    delete options.appId;
    return request({
        url: `${prefix}wx/user/${params.appId}/login`,
        data: options,
        method: 'GET'
    })
}

// 微信登录接口-获取手机号
export const GET_WEIXIN_PHONE = params => {
    return request({
        url: `${prefix}wx/user/${params.appId}/phone`,
        data: params,
        method: 'GET'
    })
}

// 新增预约
export const ADD_APPOINTMENT_INFO = params => {
    return request({
        url: `${prefix}appointment/add`,
        data: params,
        method: 'POST'
    })
}