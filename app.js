// app.js
import {
    LOGIN_BY_WEIXIN,
    GET_WEIXIN_PHONE
} from '@/http/interfaces';
App({
    onLaunch() {
        const that = this;
        // 展示本地存储能力
        // const logs = wx.getStorageSync('logs') || []
        // logs.unshift(Date.now())
        // wx.setStorageSync('logs', logs)

        // 获取系统信息
        const systemInfo = wx.getSystemInfoSync();
        const safeArea = systemInfo.safeArea || {};
        // console.log(systemInfo);
        that.globalData.navBarHeight = systemInfo.statusBarHeight + 44;
        that.globalData.statusBarHeight = systemInfo.statusBarHeight;
        that.globalData.system = systemInfo.platform;
        that.globalData.shared = wx.getStorageSync('shared');
        that.globalData.safeArea = safeArea;
        that.globalData.safeHeight = systemInfo.screenHeight - safeArea.bottom;

        // 检测小程序是否有更新
        const updateManager = wx.getUpdateManager()
        updateManager.onCheckForUpdate(function (res) {
            // 请求完新版本信息的回调
            //console.log(res.hasUpdate)
        })
        updateManager.onUpdateReady(function () {
            wx.showModal({
                title: '更新提示',
                content: '新版本已经准备好，是否重启应用？',
                success: function (res) {
                    if (res.confirm) {
                        // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                        updateManager.applyUpdate()
                    }
                }
            })
        })

        //引入公共的js
        //const Interfaces = require('http/interfaces.js');
        //const request = require('http/request.js');
        //this.globalData.Interfaces = Interfaces;
        //this.globalData.request = request;



        // 登录
        wx.login({
            success: res => {
                // 发送 res.code 到后台换取 openId, sessionKey, unionId
                // console.log(res)
                let info = wx.getAccountInfoSync();
                let appId = info.miniProgram.appId;
                let params = {}
                params.code = res.code;
                params.appId = appId;
                LOGIN_BY_WEIXIN(params).then(msg => {
                    // console.log(msg);
                    that.globalData.appId = appId;
                    that.globalData.openId = msg.data.openid;
                    that.globalData.sessionKey = msg.data.sessionKey;
                })
            }
        })
    },
    globalData: {
        userInfo: {},
        shared: false, // 是否分享
        shareId: '', // 分享ID
        navBarHeight: 0,
        statusBarHeight: 0,
        platform: '', // 机型
        alarmInfo: '',
        appId: '',
        openId: '',
        sessionKey: '',
        safeArea: {}, // 针对苹果手机 底部安全区域
        safeHeight: '', // 底部安全区域高度
    }
})