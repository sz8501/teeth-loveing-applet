// packages/inquirePackage/pages/price.js
import {
    GET_INQUIRE_PRICE
} from "@/http/interfaces";

import {
    checkPhone,
    deepClone,
    showToast
} from "@/utils/util";
import cityJson from '@/assets/json/city.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        checked: false,
        form: {},
        showDialog: false,
        loading: false,
        questionList: ['牙齿畸形', '牙齿缺失', '蛀牙'],
        userList: [],
        pickerId: "troubleName",
        cityJson: [
            [],
            []
        ],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        let list = cityJson.cityList;
        let arr = [{
            values: list,
            defaultIndex: 0
        }, {
            values: list[0].children,
            defaultIndex: 0
        }]
        this.setData({
            cityJson: arr
        })
        // this.getList()
        // this.getTeethList()
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },


    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },

    // 获取列表
    getList() {
        let _this = this;
        let params = {
            pageNumber: 1,
            pageSize: 20
        }
        GET_QUOTATION_SEARCH(params).then(res => {
            let obj = res.data || {};
            let userList = obj.list || [];
            userList.map(k => {
                k.showMobile = _this.formatPhone(k.phone)
            })
            this.setData({
                userList
            })
        })
    },

    // 手机号加密
    formatPhone(str) {
        if (!str) {
            return "";
        }
        let reg = /(\d{3})\d{4}(\d{4})/; //正则表达式
        let phone = str.replace(reg, "$1****$2");
        return phone;
    },

    // 打开弹框
    openDialog(e) {
        let id = e.currentTarget.dataset.id;
        this.setData({
            pickerId: id,
            showDialog: true
        })
    },

    // 关闭弹框
    closeDialog() {
        this.setData({
            pickerId: '',
            showDialog: false
        })
    },

    // 保存牙齿情况
    onConfirm(e) {
        console.log(e);
        let row = e.detail.value;
        let id = this.data.pickerId;
        let form = this.data.form;

        // 牙齿情况
        if (id == "dentalCondition") {
            form.dentalCondition = row;
        }

        if (id == 'cityName') {
            form.city = row[1].code
            form.cityName = row[1].name
        }

        this.setData({
            form
        })
        this.closeDialog();
    },

    // 监听多列表切换
    onChange(e) {
        const {
            picker,
            value,
            index
        } = e.detail;
        if (index == 0) {
            picker.setColumnValues(index + 1, value[index].children || []);
        }
    },

    // 输入框绑定
    inputEvent(e) {
        console.log(e)
        let id = e.currentTarget.dataset.id;
        let txt = e.detail.value;
        let form = this.data.form;
        form[id] = txt;
        this.setData({
            form: form
        })
    },

    // 获取牙齿情况
    getTeethList() {
        GET_ORALCAVITY_TROUBLE_OPTIONS().then(res => {
            let questionList = res.data || [];
            this.setData({
                questionList
            })
        })
    },

    // 保存数据
    async saveInfo() {
        let o = this.data.form;
        if (!o.cityName) {
            showToast({
                message: "请选择城市",
            });
            return false;
        }
        if (!o.dentalCondition) {
            showToast({
                message: "请选择牙齿情况",
            });
            return false;
        }
        if (!o.phoneNumber) {
            showToast({
                message: "请输入手机号码",
            });
            return false;
        }
        if (checkPhone(o.phoneNumber)) {
            showToast({
                message: "手机号码格式不正确",
            });
            return false;
        }

        if (!this.data.checked) {
            showToast({
                message: "请阅读并勾选服务条款",
            });
            return false;
        }

        this.loading = true;
        let res = await GET_INQUIRE_PRICE(o);
        this.loading = false;
        if (res.code == 200) {
            wx.navigateTo({
                url: '/pages/inquire/success',
            })
            this.setData({
                form: {}
            })
        }
    },

    // 监听协议切换
    onCheckChange(e) {
        this.setData({
            checked: e.detail
        })
    }

})