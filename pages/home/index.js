// pages/home/index.js
var app = getApp()
import {
    GET_ARTICLE_LIST,
    GET_CITY_LIST
} from '@/http/interfaces';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        cityInfo: {},
        statusBarHeight: app.globalData.statusBarHeight,
        safeHeight: app.globalData.safeHeight,
        navbarHeight: '',
        hotList: [{
                name: '深圳',
                code: ''
            },
            {
                name: '广州',
                code: ''
            },
            {
                name: '北京',
                code: ''
            },
            {
                name: '上海',
                code: ''
            },
            {
                name: '南京',
                code: ''
            },
        ],
        newsList: [],
        cityList: [],

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.setData({
            statusBarHeight: app.globalData.statusBarHeight,
            navbarHeight: app.globalData.navBarHeight,
            safeHeight: app.globalData.safeHeight,
        })
        this.getNews();
        this.getCity();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        console.log('222222222222')
        let n = wx.getStorageSync('city');
        let m = n ? JSON.parse(n) : {};
        this.setData({
            cityInfo: m
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },

    // 选择城市
    chooseCity() {
        wx.navigateTo({
            url: '/pages/city/index?type=1',
        })
    },

    // 获取最新资讯
    getNews() {
        let params = {};
        params.current = 1;
        params.size = 10;
        params.isPublished = true;
        params.isFeatured = true;
        GET_ARTICLE_LIST(params).then(res => {
            let _obj = res.data || {};
            let list = _obj.records || [];
            this.setData({
                newsList: list
            })
        })

    },

    // 快速查价
    goPrice() {
        wx.navigateTo({
            url: '/pages/inquire/price',
        })
    },

    // 新闻资讯查看详情
    lookDetail(e) {
        let row = e.currentTarget.dataset.info;
        wx.navigateTo({
            url: `/pages/news/detail?id=${row.id}`,
        })
    },

    // 查看医生
    lookDoctor(e) {
        console.log(e)
        let _type = e.currentTarget.dataset.type;
        wx.navigateTo({
            url: `/pages/city/index?type=${_type}`,
        })
    },

    // 去搜索
    goSearch() {
        wx.navigateTo({
            //url: '/pages/doctor/list?focus=true',
            url: '/pages/search/index',
        })
    },

    // 获取城市
    getCity() {
        let params = {};
        params.current = 1;
        params.size = 10;
        params.isFeatured = true;
        params.isPublished = true;
        GET_CITY_LIST(params).then(res => {
            let _obj = res.data || {};
            let list = _obj.records || [];
            this.setData({
                cityList: list
            })
        })
    },

    // 城市-去查看医生
    goDoctorList(e) {
        console.log(e)
        let row = e.currentTarget.dataset.info;
        wx.setStorageSync('city', JSON.stringify(row))
        wx.navigateTo({
            url: '/pages/doctor/list',
        })
    },

    // 查看案例
    lookCase(e) {
        console.log(e);
        let id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/pages/news/detail?id=' + id,
        })
    }

})