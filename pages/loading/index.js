// index.js
// 获取应用实例
const app = getApp()
import {
    SHARED_VEHICLE_CLICK,
    SAVE_WEIXIN_INFO,
    LOGIN_BY_AUTH
} from '../../http/interfaces'
import {
    getStorageSync
} from '../../utils/util'
Page({
    data: {
        showDialog: false
    },
    onLoad(options) {
        let _this = this;
        console.log(options)
        let token = wx.getStorageSync('token')
        let user = getStorageSync('user')

        // let oParams = {}
        // oParams.sharedId = options.shareId
        app.globalData.shareId = options.shareId
        wx.setStorageSync('sharedInfo', JSON.stringify(options))

        // 已经登录过
        if (token && user.userType == 'normal') {
            app.globalData.userInfo = user
            wx.switchTab({
                url: '/pages/tabbar/index',
            })
            return false
        }

        app.globalData.shared = options.shared
        wx.setStorageSync('shared', options.shared)

        // 未登录
        wx.login({
            success: function (res) {
                _this.loginByWeixin(res.code, options.shareId)
            }
        })
    },

    // 获取用户信息
    getInfo(e) {
        // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
        wx.getUserProfile({
            desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success: (res) => {
                console.log(res)
                let obj = res.userInfo;
                let info = app.globalData.userInfo;
                info.avatar = obj.avatarUrl
                info.nickName = obj.nickName
                app.globalData.userInfo = info;
                wx.setStorageSync('user', JSON.stringify(info))
                SAVE_WEIXIN_INFO(obj).then(res => {
                    if (res.code == 200) {
                        wx.switchTab({
                            url: '/pages/tabbar/index',
                        })
                    }
                })
            },
            fail: (error) => {
                console.log(error)
                wx.switchTab({
                    url: '/pages/tabbar/index',
                })
            }
        })
    },

    // 关闭弹框 
    onClose() {
        wx.switchTab({
            url: '/pages/tabbar/index',
        })
    },

    // 通过微信登录
    loginByWeixin(code, id) {
        let oParams = {}
        oParams.code = code;
        oParams.accessMode = 2
        LOGIN_BY_AUTH(oParams).then(res => {
            if (res.code != 200) {
                return false;
            }
            let obj = res.data || {}
            let token = obj.token
            let user = obj.user || {}
            wx.setStorageSync('token', token)
            wx.setStorageSync('user', JSON.stringify(user))
            app.globalData.userInfo = user

            // 统计用户点击分享卡片
            if (app.globalData.shareId) {
                let options = {}
                options.sharedId = id
                SHARED_VEHICLE_CLICK(options)
            }

            // 判断是否有用户头像，如果没有，则需要弹框获取用户微信信息,如果存在，则不用授权，直接跳转到分享页面
            if (!user.avatar) {
                this.setData({
                    showDialog: true
                })
            } else {
                wx.switchTab({
                    url: '/pages/tabbar/index',
                })
            }

            // wx.switchTab({
            //     url: '/pages/tabbar/index',
            // })

        })
    }

})