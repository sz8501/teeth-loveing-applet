// pages/case/detail.js
import {
    GET_DOCTOR_INFO,
    GET_CASE_INFO
} from '@/http/interfaces';
import {
    IMAGE_PATH
} from '@/http/config';
import {
    formatRichText
} from '@/utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        caseId: '',
        caseInfo: {},
        doctorInfo: {},
        doctorId: '',
        hospitalList: [],
        hosp: {},
        bannerList: [],
        current: 0,
        // swiperHeight: '150px',
        IMAGE_PATH,
        swiperHeight: 150,
        heightList: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

        if (options.id) {
            this.getInfo(options.id);
        }

        if (options.doctorId) {
            this.getDoctor(options.doctorId);
        }
        this.setData({
            doctorId: options.doctorId || '',
            caseId: options.id || ''
        })



    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {
        // let _this = this;
        // var query = wx.createSelectorQuery()
        // query.selectAll('.swiperItem').boundingClientRect(function (rect) {
        //     rect.forEach((item, index) => {
        //         console.log(item.height);
        //         _this.swiperHeight.push(item.height + 20)
        //     })
        // }).exec()

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },


    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },

    // 获取详情
    getInfo(id) {
        let params = {};
        params.id = id;
        GET_CASE_INFO(params).then(res => {
            let _obj = res.data || {};
            let arr = [];
            if (_obj.caseImageUrl) {
                arr = _obj.caseImageUrl.split(',');
            }
            _obj.caseDescription = formatRichText(_obj.caseDescription);
            this.setData({
                caseInfo: _obj,
                bannerList: arr
            })
        })
    },

    // 获取医生信息
    getDoctor(id) {
        let params = {};
        params.id = id;
        GET_DOCTOR_INFO(params).then(res => {
            let _obj = res.data || {};
            let _list = _obj.hospitalList || [];
            this.setData({
                doctorInfo: _obj,
                hospitalList: _obj.hospitalList || [],
                hosp: _list[0] || {}
            })
        })
    },

    // 去预约
    goBook() {
        let hosp = this.data.hosp || {}
        wx.navigateTo({
            url: `/pages/book/index?id=${this.data.doctorId}&hospid=${hosp.id}`,
        })

    },

    // 查看医生
    lookDoctor() {
        wx.navigateTo({
            url: `/pages/doctor/detail?id=${this.data.doctorId}`,
        })
    },

    // 监听轮播图
    onChange(e) {
        console.log(e);
        // const query = wx.createSelectorQuery()
        // query.select('#async_' + e.detail.current).boundingClientRect()
        // query.exec(function (res) {
        //     console.log(res)
        // })
        let id = `async_${e.detail.current}`;
        let m = this.data.heightList || {};
        this.setData({
            current: e.detail.current,
            swiperHeight: m[id]
        })
    },

    onImageLoad(e) {
        console.log(e);
        let id = e.currentTarget.id;
        // 获取手机的宽度
        let _width = wx.getSystemInfoSync().windowWidth

        //获取图片实际高度
        let imgheight = e.detail.height;
        //获取图片实际宽度
        let imgwidth = e.detail.width;

        let _rate = imgwidth / imgheight;

        // 计算实际高度
        let _height = parseInt(_width / _rate);
        console.log('屏幕宽度', _width)
        console.log(_height);
        let m = this.data.heightList || {};
        m[id] = _height;

        let options = {};
        options.heightList = m;

        // 针对多张图片，首次只设置第一张图的高度为轮播图的高度
        if (id == 'async_0') {
            options.swiperHeight = _height;
        }
        this.setData(options)

    }

})