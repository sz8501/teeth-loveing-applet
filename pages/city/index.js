// pages/city/index.js
import {
    GET_CITY_LIST
} from '@/http/interfaces';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        current: 1,
        size: 10,
        total: 0, // 总条数
        cityList: [],
        doctorType: '',
        searchText: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if (options.type) {
            this.setData({
                doctorType: options.type
            })
        }

        this.getList();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },


    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        // let list = this.data.cityList;
        // let total = this.data.total;
        // let current = this.data.current;
        // // 判断是否有下一页
        // if (list.length < total) {
        //     this.setData({
        //         current: current + 1
        //     })
        //     this.getList()
        // }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },

    // 获取城市列表
    getList() {
        let params = {};
        params.current = this.data.current;
        params.size = 10000;
        params.key = this.data.searchText;
        params.isPublished = true;
        GET_CITY_LIST(params).then(res => {
            let _obj = res.data || {};
            let list = _obj.records || [];
            this.setData({
                cityList: list
            })
        })
    },

    // 选择城市
    onChoose(e) {
        console.log(e);
        let row = e.currentTarget.dataset.info;
        wx.setStorageSync('city', JSON.stringify(row));

        if (this.data.doctorType) {
            wx.navigateTo({
                url: `/pages/doctor/list?type=${this.data.doctorType}`,
            })
            return false;
        }

        wx.navigateBack();
    },

    // 搜索
    onConfirm(e) {
        this.setData({
            cityList: [],
            current: 1,
            searchText: e.detail
        });
        this.getList();
    },

    // 取消搜索
    onCancel(e) {
        this.setData({
            cityList: [],
            current: 1,
            searchText: e.detail
        });
        this.getList();
    }

})