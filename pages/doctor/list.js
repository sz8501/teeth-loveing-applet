// pages/hospital/list.js
import {
    GET_DOCTOR_LIST,
    GET_CASE_LIST
} from '@/http/interfaces';
import {
    getDistance,
    judageLogin
} from '@/utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        active: 1,
        menuList: [{
                name: '矫正医生',
                id: 1
            },
            {
                name: '种植医生',
                id: 2
            },
            {
                name: '真实案例',
                id: 3
            },
        ],
        doctorList: [],
        doctorType: '',
        current: 1,
        size: 10,
        lng: '',
        lat: '',
        caseList: [],
        caseLeftList: [],
        caseRightList: [],
        searchText: '',
        total: 0,
        cityId: '',
        focus: false,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        let _this = this;
        this.setData({
            doctorType: options.type || 1,
            active: Number(options.type || 1),
            focus: options.focus || false
        })
        wx.getFuzzyLocation({
            success: function (res) {
                console.log(res);
                const lat = res.latitude;
                const lng = res.longitude;
                _this.setData({
                    lng: lng,
                    lat: lat
                })
                wx.setStorageSync('lng', lng);
                wx.setStorageSync('lat', lat);

            },
            fail: function (error) {
                console.log(error)
            },
            complete: function () {
                _this.getList();
            }
        })
        //this.getList();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        let n = wx.getStorageSync('city');
        let m = n ? JSON.parse(n) : {};
        wx.setNavigationBarTitle({
            title: m.cityName || ''
        })
        this.setData({
            cityId: m.id
        })
    },


    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        let id = this.data.active;
        let total = this.data.total;
        let list = this.data.doctorList || [];
        let current = this.data.current;
        // let cid = 'current';
        // let callBack = this.getList;
        if (id == 3) {
            //total = this.data.caseTotal;
            list = this.data.caseList || [];
            // current = this.data.caseCurrent;
            // cid = 'caseCurrent';
            // callBack = this.getCase;
        }

        // 判断是否有下一页
        if (list.length < total) {
            this.setData({
                current: current + 1
            })
            this.getList();
        }


    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },

    menuEvent(e) {
        let row = e.currentTarget.dataset.info;
        let _type = row.id;
        if (row.id < 3) {
            _type = row.id;
        }
        this.setData({
            active: row.id,
            current: 1,
            doctorList: [],
            doctorType: _type,
            caseRightList: [],
            caseLeftList: [],
            caseList: []
        })
        // if (row.id < 3) {
        //     this.getList();
        // } else {
        //     this.getCase();
        // }
        this.getList();

    },

    // 获取列表
    getList() {
        let id = this.data.active;
        if (id == 3) {
            this.getCase();
        } else {
            this.getDoctor();
        }
    },

    // 获取医生列表
    getDoctor() {
        let _this = this;
        let params = {};
        params.current = this.data.current;
        params.size = this.data.size;
        params.doctorType = this.data.doctorType;
        params.extKey = this.data.searchText;
        params.cityId = this.data.cityId;
        params.isPublished = true;
        GET_DOCTOR_LIST(params).then(res => {
            let _obj = res.data || {};
            let list = _obj.records || [];
            let arr = [];
            // 循环计算距离
            list.map(k => {
                let _hlist = k.hospitalList || [];
                let _first = _hlist[0] || {}
                if (_first.longitude) {
                    k.meter = _this.getMeter(_first.longitude, _first.latitude);
                }
                arr.push(k);
            })

            let olist = this.data.doctorList || [];
            let nList = olist.concat(arr);
            this.setData({
                doctorList: nList,
                total: _obj.total
            })
        })
    },

    // 查看详情
    lookDetail(e) {
        let isLogin = judageLogin();
        let row = e.currentTarget.dataset.info || {};
        let url = encodeURIComponent(`/pages/doctor/detail?id=${row.id}`)
        if (!isLogin) {
            wx.navigateTo({
                url: `/pages/login/index?url=${url}`,
            })
            return false;
        }
        wx.navigateTo({
            url: `/pages/doctor/detail?id=${row.id}`,
        })
    },

    // 获取案例
    getCase() {
        let params = {};
        params.current = this.data.current;
        params.size = this.data.size;
        params.isPublished = true;
        params.cityId = this.data.cityId;
        params.key = this.data.searchText;
        GET_CASE_LIST(params).then(res => {
            let _obj = res.data || {};
            let list = _obj.records || [];
            let olist = this.data.caseList || [];
            let caseLeftList = this.data.caseLeftList || [];
            let caseRightList = this.data.caseRightList || [];
            list.map((k, i) => {
                if (i % 2 == 0) {
                    caseLeftList.push(k);
                } else {
                    caseRightList.push(k);
                }
            });

            let nList = olist.concat(list);
            this.setData({
                caseList: nList,
                total: _obj.total,
                caseLeftList: caseLeftList,
                caseRightList: caseRightList
            })
        })
    },

    // 搜索
    onConfirm(e) {
        this.setData({
            doctorList: [],
            current: 1,
            searchText: e.detail,
            caseLeftList: [],
            caseRightList: [],
            caseList: []
        });
        this.getList();
    },

    // 取消搜索
    onCancel(e) {
        this.setData({
            doctorList: [],
            current: 1,
            searchText: '',
            caseLeftList: [],
            caseRightList: [],
            caseList: []
        });
        this.getList();
    },

    // 计算距离
    getMeter(lng, lat) {
        let ulng = wx.getStorageSync('lng');
        let ulat = wx.getStorageSync('lat');
        if (!lng || !lat) {
            return ''
        }

        let m = getDistance(ulng, ulat, lng, lat);
        return m;

    }


})