// pages/doctor/detail.js
import {
    GET_DOCTOR_INFO,
    GET_CASE_LIST
} from '@/http/interfaces';
import {
    showToast,
    formatRichText
} from '@/utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        active: 1,
        menuList: [{
                name: '资质',
                id: 1
            },
            {
                name: '案例',
                id: 2
            },
        ],
        showPopup: false,
        oInfo: {},
        list: [],
        hosp: {},
        doctorId: '',
        current: 1,
        size: 10,
        caseList: [],
        hospitalList: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if (options.id) {
            this.setData({
                doctorId: options.id
            })
            this.getInfo(options.id);
            this.getCase();
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },

    // 菜单点击事件
    menuEvent(e) {
        let row = e.currentTarget.dataset.info;
        this.setData({
            active: row.id
        })
    },

    // 获取医生详情
    getInfo(id) {
        let params = {};
        params.id = id;
        GET_DOCTOR_INFO(params).then(res => {
            let _obj = res.data || {};
            if (_obj.doctorTags) {
                _obj.doctorTags = _obj.doctorTags.split(',');
            } else {
                _obj.doctorTags = [];
            }
            let list = _obj.hospitalList || [];
            let hosp = list[0] || {}

            _obj.education = formatRichText(_obj.education);
            _obj.professionalTitle = formatRichText(_obj.professionalTitle);
            _obj.practiceExperience = formatRichText(_obj.practiceExperience);
            _obj.researchInterests = formatRichText(_obj.researchInterests);
            _obj.socialPositions = formatRichText(_obj.socialPositions);
            _obj.orthodonticTechnique = formatRichText(_obj.orthodonticTechnique);

            this.setData({
                oInfo: _obj,
                list: list,
                hosp: hosp
            })
        })
    },

    // 获取案例列表
    getCase() {
        let params = {};
        params.current = this.data.current;
        params.size = this.data.size;
        params.doctorId = this.data.doctorId;
        GET_CASE_LIST(params).then(res => {
            let _obj = res.data || {};
            let list = _obj.records || [];
            this.setData({
                caseList: list
            })
        })
    },

    // 去预约
    goBook() {
        console.log('555555555')
        let list = this.data.list || [];
        let hosp = this.data.hosp || {}
        console.log(list);
        if (list && list.length > 1) {
            this.setData({
                showPopup: true
            })
            return false;
        }

        wx.navigateTo({
            url: `/pages/book/index?id=${this.data.doctorId}&hospid=${hosp.id}`,
        })

    },

    // 选择医院
    chooseHospital(e) {
        let row = e.currentTarget.dataset.info || {};
        wx.navigateTo({
            url: `/pages/book/index?id=${this.data.doctorId}&hospid=${row.id}`,
        })
    },

    // 打开地图
    openMap() {
        let row = this.data.hosp;
        if (!row.longitude || !row.latitude) {
            showToast({
                message: '缺少经纬度'
            })
            return false;
        }
        let address = row.province + row.city + row.district + row.address;
        wx.openLocation({
            latitude: row.latitude,
            longitude: row.longitude,
            name: row.hospitalName,
            address: address
        })
    }

})