// pages/book/index.js
import {
    ADD_APPOINTMENT_INFO,
    GET_DOCTOR_INFO
} from '@/http/interfaces';
import {
    checkPhone,
    deepClone,
    showToast
} from "@/utils/util";
import moment from 'moment';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        showPopup: false,
        currentDate: new Date().getTime(),
        form: {},
        doctor: {},
        hospitalId: '',
        doctorId: '',
        hospital: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

        this.setData({
            hospitalId: options.hospid || '',
            doctorId: options.id || ''
        })

        if (options.id) {
            this.getInfo(options.id);
        }


    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },


    // 打开日期弹框
    openDialog() {
        this.setData({
            showPopup: true
        })
    },

    /**
     * 关闭弹框
     */
    onClose() {
        this.setData({
            showPopup: false
        })
    },

    // 保存数据
    onConfirm(e) {
        console.log(e);
        let m = this.data.form;
        m.appointmentTime = moment(e.detail).format('YYYY-MM-DD HH:mm:ss');
        this.setData({
            showPopup: false,
            currentDate: e.detail,
            form: m
        })
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },

    // 输入框绑定
    inputEvent(e) {
        console.log(e)
        let id = e.currentTarget.dataset.id;
        let txt = e.detail.value;
        let form = this.data.form;
        form[id] = txt;
        this.setData({
            form: form
        })
    },

    // 新增预约
    saveInfo() {
        let m = this.data.doctor;
        let params = this.data.form;

        if (!params.patientName) {
            showToast({
                message: '请输入姓名'
            })
            return false;
        }

        if (!params.phoneNumber) {
            showToast({
                message: '请输入电话'
            })
            return false;
        }

        if (!params.phoneNumber) {
            showToast({
                message: '请输入电话'
            })
            return false;
        }

        if (checkPhone(params.phoneNumber)) {
            showToast({
                message: "电话号码格式不正确",
            });
            return false;
        }

        if (!params.appointmentTime) {
            showToast({
                message: '请选择时间'
            })
            return false;
        }

        params.doctorName = m.doctorName;
        params.doctorId = this.data.doctorId;
        params.hospitalId = this.data.hospitalId;
        params.authorizationNumber = wx.getStorageSync('mobile');
        ADD_APPOINTMENT_INFO(params).then(res => {
            if (res.code == 200) {
                wx.navigateTo({
                    url: '/pages/book/success',
                })
            }
        })

    },

    // 获取医生详情
    getInfo(id) {
        let params = {};
        params.id = id;
        GET_DOCTOR_INFO(params).then(res => {
            let _obj = res.data || {};
            this.setData({
                doctor: _obj
            })
            this.getHospital(_obj.hospitalList || []);
        })
    },

    getHospital(list) {
        let id = this.data.hospitalId;
        let m = list.find(k => k.id == id);
        this.setData({
            hospital: m || {}
        })
    }

})