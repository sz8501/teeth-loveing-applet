// pages/news/detail.js
var app = getApp()
import {
    GET_ARTICLE_INFO
} from '@/http/interfaces';
import {
    formatRichText
} from '@/utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        safeHeight: app.globalData.safeHeight,
        isLogin: true, // 是否登录
        newsInfo: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.setData({
            safeHeight: app.globalData.safeHeight
        })
        if (options.id) {
            this.getInfo(options.id)
        }
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        // let mobile = wx.getStorageSync('mobile');
        // let time = new Date().getTime(); // 获取当前时间的毫秒数
        // let expiredTime = wx.getStorageSync('expiredTime');
        // let isLogin = false;

        // // 1、未登录  expiredTime是否存在，如果未登录+ 当前时间小于失效时间，则跳转到详情页面， 未登录+当前时间大于失效时间  则跳转登录页面
        // if (!mobile) {
        //     // 未登录 + 当前时间大于失效时间
        //     if (expiredTime && time > expiredTime) {
        //         isLogin = false;
        //     } else {
        //         isLogin = true;
        //     }
        // } else {
        //     isLogin = true;
        // }
        // console.log(mobile);
        // console.log(time > expiredTime)
        // console.log(isLogin);
        // this.setData({
        //     isLogin: isLogin
        // })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },


    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },

    // 获取详情
    getInfo(id) {
        let params = {};
        params.id = id
        GET_ARTICLE_INFO(params).then(res => {
            let _obj = res.data || {};
            _obj.articleContent = formatRichText(_obj.articleContent);
            this.setData({
                newsInfo: _obj
            })
        })
    },

    // 去登录
    goLogin() {
        wx.navigateTo({
            url: '/pages/login/index',
        })
    }
})