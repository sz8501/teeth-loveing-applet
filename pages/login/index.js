// pages/login/index.js
var app = getApp()
import {
    LOGIN_BY_WEIXIN,
    GET_WEIXIN_PHONE
} from '@/http/interfaces';
import {
    showModal
} from '@/utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        appId: '',
        backUrl: '',
        checked: false,
        loginTimes: 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        let info = wx.getAccountInfoSync();
        let appId = info.miniProgram.appId;
        console.log(appId);
        this.setData({
            appId: appId,
            backUrl: options.url || ''
        })


    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        let loginTimes = Number(wx.getStorageSync('loginTimes'));
        this.setData({
            loginTimes: loginTimes
        })
    },


    onShareAppMessage: function () {
        // 页面被用户分享时执行
    },


    // 输入框绑定方法
    inputEvent(e) {

    },

    // 获取登录信息
    getLogin() {
        let _this = this;
        return new Promise((resolve, reject) => {
            wx.login({
                success: function (res) {
                    console.log(res)
                    let params = {};
                    params.appId = _this.data.appId;
                    params.code = res.code;
                    LOGIN_BY_WEIXIN(params).then(msg => {
                        resolve(msg.data)
                    })
                }
            })
        })

    },

    // 获取手机号码
    getPhoneNumber(e) {
        console.log(e);
        let _this = this;
        let m = e.detail || {};
        let params = {};
        // let res = await this.getLogin();
        if (!m.code) {
            // showModal({
            //     message: 'AES解密失败!',
            //     showCancel: false,
            //     success: function () {
            //         wx.reLaunch({
            //             url: '/pages/home/index',
            //         })
            //     }
            // })
            wx.reLaunch({
                url: '/pages/home/index',
            })
            return false;
        }

        params.appId = app.globalData.appId;
        params.rawData = '';
        params.encryptedData = m.encryptedData;
        params.iv = m.iv;
        params.openid = app.globalData.openId;
        params.sessionKey = app.globalData.sessionKey;
        GET_WEIXIN_PHONE(params).then(response => {
            if (response.code == 200) {
                let _obj = response.data || {};
                wx.setStorageSync('mobile', _obj.phoneNumber);
                wx.setStorageSync('user', JSON.stringify(_obj));
                wx.removeStorageSync('expiredTime');
                wx.removeStorageSync('loginTimes');
                if (_this.data.backUrl) {
                    wx.redirectTo({
                        url: decodeURIComponent(_this.data.backUrl),
                    })
                    return false;
                }
                wx.navigateBack();
            }
        })


    },

    // 查看协议
    lookContract() {
        wx.navigateTo({
            url: '/pages/contract/index',
        })
    },

    // 暂时不登录-先逛逛  以当前时间缓存2分钟，2分钟内不弹出登录
    noLogin() {
        let time = new Date().getTime(); // 当前时间的毫秒数
        let expiredTime = time + 2 * 60 * 1000; // 失效时间
        let times = Number(wx.getStorageSync('loginTimes')); // 不登录次数
        // if(times){

        // }
        console.log(times);
        wx.setStorageSync('expiredTime', expiredTime);
        wx.setStorageSync('loginTimes', times + 1);
        wx.navigateBack();
        console.log(time + '-----' + expiredTime);
    },

    onChange(e) {
        let checked = this.data.checked;
        this.setData({
            checked: !checked
        })
    },

    checkAgreement() {
        let checked = this.data.checked;
        if (!checked) {
            wx.showToast({
                title: '请勾选和阅读用户协议',
                icon: 'none'
            })
        }
    }

})