// pages/search/index.js
import {
    GET_DOCTOR_LIST,
    GET_CASE_LIST
} from '@/http/interfaces';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        searchText: '',
        history: [],
        hotList: [],
        doctorList: [],
        caseList: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.getHot();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        let list = wx.getStorageSync('history');
        this.setData({
            history: list || []
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },


    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },

    // 取消搜索
    onCancel(e) {
        console.log(e);
        let txt = e.detail;
        this.setData({
            searchText: txt,
            doctorList: []
        })
        //this.getList();
        //wx.navigateBack();
    },

    // 搜索
    onConfirm(e) {
        this.setData({
            current: 1,
            searchText: e.detail
        });
        this.getList();
    },

    // 查询医生列表
    getList() {
        let params = {};
        params.extKey = this.data.searchText;
        params.current = 1;
        params.size = 10000;
        params.isPublished = true;
        GET_DOCTOR_LIST(params).then(res => {
            if (res.code == 200) {
                let _obj = res.data || {};
                let list = _obj.records || [];
                // let first = list[0] || {};
                let history = wx.getStorageSync('history') || [];
                wx.setStorageSync('history', history);
                this.setData({
                    doctorList: list
                })
                // if (first && first.id) {
                //     history.push(first);
                //     wx.setStorageSync('history', history);
                //     wx.navigateTo({
                //         url: `/pages/doctor/detail?id=${first.id}`,
                //     })
                // }
            }
        })
    },

    // 获取案例
    getCase() {
        let params = {};
        params.key = this.data.searchText;
        params.current = 1;
        params.size = 10000;
        GET_CASE_LIST(params).then(res => {
            let _obj = res.data || {};
            let list = _obj.records || [];
            this.setData({
                caseList: list
            })
        })
    },


    // 获取热门访问
    getHot() {
        let params = {};
        params.current = 1;
        params.size = 10000;
        params.isFeatured = true;
        GET_DOCTOR_LIST(params).then(res => {
            if (res.code == 200) {
                let _obj = res.data || {};
                let list = _obj.records || [];
                this.setData({
                    hotList: list
                })
            }
        })
    },

    // 查看医生
    lookDoctor(e) {
        let row = e.currentTarget.dataset.info || {};
        wx.navigateTo({
            url: `/pages/doctor/detail?id=${row.id}`,
        })
    },

    // 更新值
    onUpdate(e) {
        console.log(e);
        this.setData({
            searchText: e.detail
        })
    }

})